#! /usr/bin/ruby

# Copyright (c) 2008 David Leverton <levertond@googlemail.com>
# Based in part upon 'cleanup_metadata.rb', which is:
#     Copyright (c) 2008 Ingmar Vanhassel <ingmar@exherbo.org>
#
# This program is is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License, version
# 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA

require 'Paludis'

require 'getoptlong'
require 'fileutils'

def die(msg)
    $stderr.puts "#$0: #{msg}"
    exit 1
end

Paludis::Log.instance.program_name = $0

envspec = ""

GetoptLong.new(
    [ '--log-level',         GetoptLong::REQUIRED_ARGUMENT ],
    [ '--environment', '-E', GetoptLong::REQUIRED_ARGUMENT ]
).each do | opt, arg |
    case opt

    when '--log-level'
        Paludis::Log.instance.log_level = case arg
            when 'silent'   then  Paludis::LogLevel::Silent
            when 'warning'  then  Paludis::LogLevel::Warning
            when 'qa'       then  Paludis::LogLevel::Qa
            when 'debug'    then  Paludis::LogLevel::Debug
            else die "invalid #{opt}: #{arg}"
        end

    when '--environment'
        envspec = arg

    end
end

env = Paludis::EnvironmentFactory.instance.create(envspec)

write_caches = {}
write_cache_repos = {}

env.repositories do | repo |
    next unless repo["format"].parse_value == "ebuild"
    write_cache = repo["write_cache"].parse_value
    next if write_cache == "/var/empty"
    write_cache_repo = "#{write_cache}/#{repo.name}"
    write_caches[write_cache] = write_cache_repos[write_cache_repo] = true
    next unless File.exists?(write_cache_repo)

    Dir.foreach(write_cache_repo) do | cat |
        next if [".", ".."].include?(cat)
        write_cache_repo_cat = "#{write_cache_repo}/#{cat}"

        cache_inuse = env[Paludis::Selection::AllVersionsUnsorted.new(
                              Paludis::Generator::InRepository.new(repo.name) &
                              Paludis::Generator::Category.new(cat))].inject({}) {
            | h, id | h["#{id.name.package}-#{id.version}"] = h }

        Dir.foreach(write_cache_repo_cat) do | file |
            next if [".", ".."].include?(file)
            File.unlink("#{write_cache_repo_cat}/#{file}") unless cache_inuse[file]
        end
        Dir.rmdir(write_cache_repo_cat) if cache_inuse.empty?
    end
end

write_caches.each_key do | write_cache |
    next unless File.exists?(write_cache)
    Dir.foreach(write_cache) do | repo_name |
        next if [".", ".."].include?(repo_name)
        write_cache_repo = "#{write_cache}/#{repo_name}"
        FileUtils.rm_r(write_cache_repo) unless !File.directory?(write_cache_repo) || write_cache_repos[write_cache_repo]
    end
end

# vim: set sw=4 ts=4 tw=100 et :
