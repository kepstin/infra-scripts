#!/bin/sh

set -e

echo "Setting up CI environment"
source /etc/profile
eclectic env update

chgrp paludisbuild /dev/tty

# make sure that we build generic binaries
cat <<EOF > /etc/paludis/bashrc
CHOST="x86_64-pc-linux-gnu"
i686_pc_linux_gnu_CFLAGS="-march=x86-64 -mtune=generic -pipe -O2"
i686_pc_linux_gnu_CXXFLAGS="-march=x86-64 -mtune=generic -pipe -O2"
x86_64_pc_linux_gnu_CFLAGS="-march=x86-64 -mtune=generic -pipe -O2"
x86_64_pc_linux_gnu_CXXFLAGS="-march=x86-64 -mtune=generic -pipe -O2"
EOF

# docker build does not allow to add the SYS_PTRACE cap
export PALUDIS_DO_NOTHING_SANDBOXY=1

# this will need to be provided from the runner eventually
sed -i 's/jobs=2/jobs=5/g' /etc/paludis/options.conf

# options.conf.d for temporary options
mkdir -v /etc/paludis/options.conf.d
echo '*/* build_options: -recommended_tests' >> /etc/paludis/options.conf.d/disabled-tests.conf

echo "sys-apps/paludis ruby" >> /etc/paludis/options.conf
cave resolve sys-apps/paludis dev-ruby/ruby-elf -x

# Remove temporary options
rm -rf /etc/paludis/options.conf.d/

echo "Downloading build scripts"
cd /usr/local/bin
wget -c https://git.exherbo.org/infra-scripts.git/plain/continuous-integration/gitlab/buildtest
wget -c https://git.exherbo.org/infra-scripts.git/plain/continuous-integration/gitlab/handle_confirmations
wget -c https://git.exherbo.org/exherbo-dev-tools.git/plain/mscan2.rb
wget -c https://git.exherbo.org/exherbo-dev-tools.git/plain/check_slots
chmod +x buildtest handle_confirmations mscan2.rb check_slots

echo "Cleaning up again"
rm -f /build.sh
rm -f /root/.bash_history
rm -Rf /tmp/*
rm -Rf /var/tmp/paludis/build/*
rm -Rf /var/cache/paludis/distfiles/*
rm -Rf /var/log/paludis/*
rm -f /var/log/paludis.log

