CPN_SKIP_TESTS+=(
    # Fails a test in CI environment
    # Last checked: 15 Aug 2022 (version: 4.0.0)
    # Context:
    # Running /var/tmp/paludis/build/sys-process-procps-4.0.0/work/procps-ng-4.0.0/testsuite/free.test/free.exp ...
    # FAIL: free with commit
    sys-process/procps

    # Fails a test in CI environment
    # Last checked: 07 Oct 2022 (version: 2.38.1)
    # Context:
    # lsfd: read-only regular file                        ... FAILED (lsfd/mkfds-ro-regular-file)
    #
    # This test failed repeatably on three stage builds in our CI environment,
    # but it works reliably when I do a stage build locally
    sys-apps/util-linux
)
